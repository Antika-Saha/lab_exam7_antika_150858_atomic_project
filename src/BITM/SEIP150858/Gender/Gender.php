<?php

namespace App\Gender;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Gender extends DB
{

    public $id = "";

    public $name = "users";

    public $gender = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name=$postVariableData['name'];
        }

        if(array_key_exists('gender',$postVariableData)){
            $this->gender=$postVariableData['gender'];
        }

    }

    public function store(){


        $arrData = array($this->name,$this->gender);
        $sql = "Insert INTO gender(name,gender) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }//end of store


    public function index()
    {

    }


}// end of BookTitle class