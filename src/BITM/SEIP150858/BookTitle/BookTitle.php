<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB
{

    public $id = "";

    public $book_title = "users";

    public $author_name = "";


    public function __construct()
    {
        parent::__construct();
    }




    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('book_title',$postVariableData)){
            $this->book_title=$postVariableData['book_title'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name=$postVariableData['author_name'];
        }

    }

public function store(){


    $arrData = array($this->book_title,$this->author_name);
    $sql = "Insert INTO book_title(book_title, author_name) VALUES (?,?)";

    $STH = $this->DBH->prepare($sql);

   $result= $STH->execute($arrData);

    if($result)
        Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
    else
    Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

    Utility::redirect('creat.php');

}//end of store





    public function index()
    {

    }


}// end of BookTitle class