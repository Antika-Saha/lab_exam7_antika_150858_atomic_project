<?php

namespace App\City;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

use PDO;

class City extends DB
{

    public $id = "";

    public $name = "users";

    public $city_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name=$postVariableData['name'];
        }

        if(array_key_exists('city_name',$postVariableData)){
            $this->city_name=$postVariableData['city_name'];
        }

    }

    public function store(){


        $arrData = array($this->name,$this->city_name);
        $sql = "Insert INTO city(name,city_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }//end of store

    public function index()
    {

    }


}// end of BookTitle class