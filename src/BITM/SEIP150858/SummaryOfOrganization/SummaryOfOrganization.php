<?php

namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class SummaryOfOrganization extends DB
{

    public $id = "";

    public $org_name = "users";

    public $summary_org = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('org_name',$postVariableData)){
            $this->org_name=$postVariableData['org_name'];
        }

        if(array_key_exists('summary_org',$postVariableData)){
            $this->summary_org=$postVariableData['summary_org'];
        }

    }

    public function store(){


        $arrData = array($this->org_name,$this->summary_org);
        $sql = "Insert INTO summary_of_organization(org_name, summary_org) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }

    public function index()
    {

    }


}// end of BookTitle class