<?php

namespace App\Hobbies;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Hobbies extends DB
{

    public $id = "";

    public $name = "users";

    public $hobbies = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name=$postVariableData['name'];
        }

        if(array_key_exists('hobbies',$postVariableData)){
            $this->hobbies=implode(",",$postVariableData['hobbies']);
        }

    }

    public function store(){


        $arrData = array($this->name,$this->hobbies);
        $sql = "Insert INTO hobbies(name,hobbies) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }//end of store


    public function index()
    {

    }


}// end of BookTitle class